package br.edu.facef.business;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.edu.facef.dao.ClienteDAO;
import br.edu.facef.model.Cliente;

@RunWith(MockitoJUnitRunner.class)
public class ClienteBusinessTest {

	@Mock
	private ClienteDAO clienteDAO;

	@Test
	public void salvarClienteSucesso() {

		Cliente clienteMock = new Cliente();
		clienteMock.setId(Long.valueOf(99));
		clienteMock.setNome("Maria da Silva");
		clienteMock.setCidade("Monte Santo de Minas");
		clienteMock.setEstado("MG");

		Cliente novoCliente = new Cliente();
		novoCliente.setNome("Maria da Silva");
		novoCliente.setEstado("MG");
		novoCliente.setCidade("Monte Santo de Minas");

		Mockito.when(clienteDAO.salvar(novoCliente)).thenReturn(clienteMock);
		ClienteBusiness clienteBusiness = new ClienteBusiness(clienteDAO);
		Cliente clienteSalvo = clienteBusiness.salvarCliente(novoCliente);
		assertEquals(clienteMock, clienteSalvo);
	}

	@Test
	public void salvarClienteComErro() {
		Cliente novoCliente = new Cliente();
		novoCliente.setNome("");
		novoCliente.setEstado("MG");
		novoCliente.setCidade("Monte Santo de Minas");

		ClienteBusiness clienteBusiness = new ClienteBusiness(clienteDAO);
		
		try {
			Cliente clienteSalvo = clienteBusiness.salvarCliente(novoCliente);
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(),"Nome do cliente invalido!");
		}
		
	}
	
	@Test
	public void deletarCliente() {
		Cliente novoCliente = new Cliente();
		novoCliente.setId(Long.valueOf(99));
		novoCliente.setNome("Maria da Silva");
		novoCliente.setEstado("MG");
		novoCliente.setCidade("Monte Santo de Minas");

		ClienteBusiness clienteBusiness = new ClienteBusiness(clienteDAO);
		Cliente clienteSalvo = clienteBusiness.deletarCliente(novoCliente);
		assertEquals(null, clienteSalvo);
	}
	
	@Test 
	public void editarCliente() {
		
		Cliente clienteMock = new Cliente();
		clienteMock.setId(Long.valueOf(99));
		clienteMock.setNome("Jo�o Marcos");
		clienteMock.setCidade("Franca");
		clienteMock.setEstado("SP");

		Cliente novoCliente = new Cliente();
		novoCliente.setId(Long.valueOf(99));
		novoCliente.setNome("Maria da Silva");
		novoCliente.setEstado("MG");
		novoCliente.setCidade("Monte Santo de Minas");
		
		Mockito.when(clienteDAO.editar(novoCliente)).thenReturn(clienteMock);
		ClienteBusiness clienteBusiness = new ClienteBusiness(clienteDAO);
		Cliente clienteSalvo = clienteBusiness.editarCliente(novoCliente);
		assertEquals(clienteMock, clienteSalvo);
	}
	
}
