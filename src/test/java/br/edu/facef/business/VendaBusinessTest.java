package br.edu.facef.business;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.edu.facef.dao.VendaDAO;
import br.edu.facef.model.Cliente;
import br.edu.facef.model.Empresa;
import br.edu.facef.model.Produto;
import br.edu.facef.model.Venda;
import br.edu.facef.model.Vendedor;

@RunWith(MockitoJUnitRunner.class)
public class VendaBusinessTest {

	@Mock
	private VendaDAO vendaDAO;

	@Test
	public void testSalvarVenda() {
		Venda vendaMock = new Venda();

		vendaMock.setId(999l);

		Cliente cliente = new Cliente();
		cliente.setId(Long.valueOf(99));
		cliente.setNome("Maria da Silva");
		cliente.setCidade("Monte Santo de Minas");
		cliente.setEstado("MG");

		vendaMock.setCliente(cliente);

		Empresa empresa = new Empresa();
		empresa.setId(111l);
		empresa.setNome("Irm�es da Silva");
		empresa.setCnpj("1234566");
		empresa.setTelefone("1234566");

		vendaMock.setEmpresa(empresa);

		Produto produto = new Produto();
		produto.setId(111l);
		produto.setNome("Notebook Asus");
		produto.setPreco(2000.00);
		produto.setQuantidade(10);

		vendaMock.setProduto(produto);

		Vendedor vendedor = new Vendedor();
		vendedor.setId(111l);
		vendedor.setCpf("803.872.940-40");
		vendedor.setNome("David Stefani Prado");
		vendedor.setTelefone("16994151771");

		vendaMock.setVendedor(vendedor);
		vendaMock.setDataVenda(new Date());

		Venda novoVenda = new Venda();

		novoVenda.setId(999l);

		Cliente clienteNovo = new Cliente();
		clienteNovo.setId(Long.valueOf(99));
		clienteNovo.setNome("Maria da Silva");
		clienteNovo.setCidade("Monte Santo de Minas");
		clienteNovo.setEstado("MG");

		novoVenda.setCliente(cliente);

		Empresa empresaNova = new Empresa();
		empresaNova.setId(111l);
		empresaNova.setNome("Irm�es da Silva");
		empresaNova.setCnpj("1234566");
		empresaNova.setTelefone("1234566");

		novoVenda.setEmpresa(empresa);

		Produto produtoNovo = new Produto();
		produtoNovo.setId(111l);
		produtoNovo.setNome("Notebook Asus");
		produtoNovo.setPreco(2000.00);
		produtoNovo.setQuantidade(10);

		novoVenda.setProduto(produto);

		Vendedor vendedorNovo = new Vendedor();
		vendedorNovo.setId(111l);
		vendedorNovo.setCpf("803.872.940-40");
		vendedorNovo.setNome("David Stefani Prado");
		vendedorNovo.setTelefone("16994151771");

		novoVenda.setVendedor(vendedor);
		novoVenda.setDataVenda(new Date());

		Mockito.when(vendaDAO.salvar(novoVenda)).thenReturn(vendaMock);
		VendaBusiness vendaBusiness = new VendaBusiness(vendaDAO);
		Venda vendaSalvo = vendaBusiness.salvarVenda(novoVenda);
		assertEquals(vendaMock, vendaSalvo);
	}

	@Test
	public void editarVenda() {
		Venda vendaMock = new Venda();
		vendaMock.setId(999l);

		Cliente cliente = new Cliente();
		cliente.setId(Long.valueOf(99));
		cliente.setNome("Maria da Silva");
		cliente.setCidade("Monte Santo de Minas");
		cliente.setEstado("MG");

		vendaMock.setCliente(cliente);

		Empresa empresa = new Empresa();
		empresa.setId(111l);
		empresa.setNome("Irm�es da Silva");
		empresa.setCnpj("1234566");
		empresa.setTelefone("1234566");

		vendaMock.setEmpresa(empresa);

		Produto produto = new Produto();
		produto.setId(111l);
		produto.setNome("Notebook Asus");
		produto.setPreco(2000.00);
		produto.setQuantidade(10);

		vendaMock.setProduto(produto);

		Vendedor vendedor = new Vendedor();
		vendedor.setId(111l);
		vendedor.setCpf("803.872.940-40");
		vendedor.setNome("David Stefani Prado");
		vendedor.setTelefone("16994151771");

		vendaMock.setVendedor(vendedor);
		vendaMock.setDataVenda(new Date());

		Venda novoVenda = new Venda();

		novoVenda.setId(999l);

		Cliente clienteNovo = new Cliente();
		clienteNovo.setId(Long.valueOf(99));
		clienteNovo.setNome("Maria da Silva");
		clienteNovo.setCidade("Monte Santo de Minas");
		clienteNovo.setEstado("MG");

		novoVenda.setCliente(cliente);

		Empresa empresaNova = new Empresa();
		empresaNova.setId(111l);
		empresaNova.setNome("Irm�es da Silva");
		empresaNova.setCnpj("1234566");
		empresaNova.setTelefone("1234566");

		novoVenda.setEmpresa(empresa);

		Produto produtoNovo = new Produto();
		produtoNovo.setId(111l);
		produtoNovo.setNome("Notebook Asus");
		produtoNovo.setPreco(2000.00);
		produtoNovo.setQuantidade(10);

		novoVenda.setProduto(produto);

		Vendedor vendedorNovo = new Vendedor();
		vendedorNovo.setId(111l);
		vendedorNovo.setCpf("803.872.940-40");
		vendedorNovo.setNome("David Stefani Prado");
		vendedorNovo.setTelefone("16994151771");

		novoVenda.setVendedor(vendedor);
		novoVenda.setDataVenda(new Date());

		Mockito.when(vendaDAO.editar(novoVenda)).thenReturn(vendaMock);
		VendaBusiness vendaBusiness = new VendaBusiness(vendaDAO);
		Venda vendaSalvo = vendaBusiness.editarVenda(novoVenda);
		assertEquals(vendaMock, vendaSalvo);
	}

	@Test
	public void deletarVenda() {
		Venda venda = new Venda();
		venda.setId(999l);

		Cliente cliente = new Cliente();
		cliente.setId(Long.valueOf(99));
		cliente.setNome("Maria da Silva");
		cliente.setCidade("Monte Santo de Minas");
		cliente.setEstado("MG");

		venda.setCliente(cliente);

		Empresa empresa = new Empresa();
		empresa.setId(111l);
		empresa.setNome("Irm�es da Silva");
		empresa.setCnpj("1234566");
		empresa.setTelefone("1234566");

		venda.setEmpresa(empresa);

		Produto produto = new Produto();
		produto.setId(111l);
		produto.setNome("Notebook Asus");
		produto.setPreco(2000.00);
		produto.setQuantidade(10);

		venda.setProduto(produto);

		Vendedor vendedor = new Vendedor();
		vendedor.setId(111l);
		vendedor.setCpf("803.872.940-40");
		vendedor.setNome("David Stefani Prado");
		vendedor.setTelefone("16994151771");

		venda.setVendedor(vendedor);
		venda.setDataVenda(new Date());

		Mockito.when(vendaDAO.excluir(venda)).thenReturn(null);
		VendaBusiness vendaBusiness = new VendaBusiness(vendaDAO);
		Venda vendaDeletada = vendaBusiness.editarVenda(venda);
		assertEquals(null, vendaDeletada);
	}

	@Test
	public void salvarClienteComErro() {
		Venda novoVenda = new Venda();
		novoVenda.setId(999l);

		Cliente cliente = new Cliente();
		novoVenda.setCliente(cliente);

		Empresa empresa = new Empresa();
		empresa.setId(111l);
		empresa.setNome("Irm�es da Silva");
		empresa.setCnpj("1234566");
		empresa.setTelefone("1234566");

		novoVenda.setEmpresa(empresa);

		Produto produto = new Produto();
		produto.setId(111l);
		produto.setNome("Notebook Asus");
		produto.setPreco(2000.00);
		produto.setQuantidade(10);

		novoVenda.setProduto(produto);

		Vendedor vendedor = new Vendedor();
		vendedor.setId(111l);
		vendedor.setCpf("803.872.940-40");
		vendedor.setNome("David Stefani Prado");
		vendedor.setTelefone("16994151771");

		novoVenda.setVendedor(vendedor);
		novoVenda.setDataVenda(new Date());

		VendaBusiness vendaBusiness = new VendaBusiness(vendaDAO);

		try {
			vendaBusiness.salvarVenda(novoVenda);
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(), "Cliente da venda invalido!");
		}

	}

}
