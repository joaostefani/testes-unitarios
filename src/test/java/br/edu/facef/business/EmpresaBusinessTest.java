package br.edu.facef.business;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.edu.facef.dao.EmpresaDAO;
import br.edu.facef.model.Empresa;

@RunWith(MockitoJUnitRunner.class)
public class EmpresaBusinessTest {

	@Mock
	private EmpresaDAO empresaDAO;
	
	@Test
	public void salvarEmpresaSucesso() {
		Empresa empresaMock = new Empresa();
		empresaMock.setId(111l);
		empresaMock.setNome("Irm�es da Silva");
		empresaMock.setCnpj("1234566");
		empresaMock.setTelefone("1234566");
		
		Empresa novaEmpresa = new Empresa();
		novaEmpresa.setNome("Irm�es da Silva 2");
		novaEmpresa.setCnpj("654321");
		novaEmpresa.setTelefone("654321");
		
		Mockito.when(empresaDAO.salvar(novaEmpresa)).thenReturn(empresaMock);
		
		EmpresaBusiness empresaBusiness = new EmpresaBusiness(empresaDAO);
		
		Empresa empresaSalva = empresaBusiness.salvarEmpresa(novaEmpresa);
				
		assertEquals(empresaMock.getId(), empresaSalva.getId());
	}
	
	@Test
	public void salvarEmpresaErroNomeVazio() {
		Empresa novaEmpresa = new Empresa();
		novaEmpresa.setNome("");
		novaEmpresa.setCnpj("654321");
		novaEmpresa.setTelefone("654321");
		
		EmpresaBusiness empresaBusiness = new EmpresaBusiness(empresaDAO);
		
		try {
			empresaBusiness.salvarEmpresa(novaEmpresa);
		} catch(RuntimeException e) {
			assertEquals(e.getMessage(), "Nome requerido.");
		}
	}
	
	@Test
	public void salvarEmpresaErroCnpjVazio() {
		Empresa novaEmpresa = new Empresa();
		novaEmpresa.setNome("Irm�es da Silva");
		novaEmpresa.setCnpj("");
		novaEmpresa.setTelefone("654321");
		
		EmpresaBusiness empresaBusiness = new EmpresaBusiness(empresaDAO);
		
		try {
			empresaBusiness.salvarEmpresa(novaEmpresa);
		} catch(RuntimeException e) {
			assertEquals(e.getMessage(), "CNPJ requerido.");
		}
	}
	
	@Test
	public void deletarEmpresa() {
		Empresa empresaMock = null;
		
		Empresa novaEmpresa = new Empresa();
		novaEmpresa.setId(Long.valueOf(99));
		novaEmpresa.setNome("Irm�es da Silva");
		novaEmpresa.setCnpj("54321");
		novaEmpresa.setTelefone("654321");
		
		Mockito.when(empresaDAO.excluirEmpresa(novaEmpresa)).thenReturn(empresaMock);
		
		EmpresaBusiness empresaBusiness = new EmpresaBusiness(empresaDAO);
		
		Empresa empresaDeleta = empresaBusiness.deletarEmpresa(novaEmpresa);
		
		assertEquals(empresaMock, empresaDeleta);
	}
}
