package br.edu.facef.business;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.edu.facef.dao.VendedorDAO;
import br.edu.facef.model.Vendedor;

@RunWith(MockitoJUnitRunner.class)
public class VendedorBusinessTest {

	@Mock
	private VendedorDAO vendedorDAO;
	
	@Test
	public void salvarVendedorSucesso() {
		Vendedor vendedorMock = new Vendedor();
		vendedorMock.setId(111l);
		vendedorMock.setNome("Pedro Oliveira");
		vendedorMock.setCpf("1234566");
		vendedorMock.setTelefone("1234566");
		
		Vendedor novoVendedor = new Vendedor();
		novoVendedor.setNome("Pedro Oliveira");
		novoVendedor.setCpf("1234566");
		novoVendedor.setTelefone("1234566");
		
		Mockito.when(vendedorDAO.salvar(novoVendedor)).thenReturn(vendedorMock);
		
		VendedorBusiness vendedorBusiness = new VendedorBusiness(vendedorDAO);
		
		Vendedor vendedorSalva = vendedorBusiness.salvarVendedor(novoVendedor);
		
		assertEquals(vendedorMock.getId(), vendedorSalva.getId());
		
	}
	
	@Test
	public void salvarVendedorErroNomeVazio() {
		Vendedor novoVendedor = new Vendedor();
		novoVendedor.setNome("Pedro Oliveira");
		novoVendedor.setCpf("1234566");
		novoVendedor.setTelefone("1234566");
		
		VendedorBusiness vendedorBusiness = new VendedorBusiness(vendedorDAO);
		
		try {
			vendedorBusiness.salvarVendedor(novoVendedor);
		} catch(RuntimeException e) {
			assertEquals(e.getMessage(), "Nome requerido.");
		}
	}
	
	@Test
	public void salvarVendedorErroCpfVazio() {
		Vendedor novoVendedor = new Vendedor();
		novoVendedor.setNome("Pedro Oliveira");
		novoVendedor.setCpf("");
		novoVendedor.setTelefone("1234566");
		
		VendedorBusiness vendedorBusiness = new VendedorBusiness(vendedorDAO);
		
		try {
			vendedorBusiness.salvarVendedor(novoVendedor);
		} catch(RuntimeException e) {
			assertEquals(e.getMessage(), "CPF requerido.");
		}
	}
	
	@Test
	public void deletarVendedor() {
		Vendedor vendedorMock = null;
		
		Vendedor novoVendedor = new Vendedor();
		novoVendedor.setId(Long.valueOf(99));
		novoVendedor.setNome("Pedro Oliveira");
		novoVendedor.setCpf("1234566");
		novoVendedor.setTelefone("1234566");
		
		Mockito.when(vendedorDAO.excluirVendedor(novoVendedor)).thenReturn(vendedorMock);
		
		VendedorBusiness vendedorBusiness = new VendedorBusiness(vendedorDAO);
		
		Vendedor vendedorDeleta = vendedorBusiness.deletarVendedor(novoVendedor);
		
		assertEquals(vendedorMock, vendedorDeleta);
	}
}
