package br.edu.facef.business;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.edu.facef.dao.ProdutoDAO;
import br.edu.facef.model.Produto;

@RunWith(MockitoJUnitRunner.class)
public class ProdutoBusinessTest {

	@Mock
	private ProdutoDAO produtoDAO;
	
	@Test
	public void salvarProdutoSucesso() {
		Produto produtoMock = new Produto();
		produtoMock.setId(111l);
		produtoMock.setNome("Coca-cola");
		produtoMock.setPreco(13.50);
		produtoMock.setQuantidade(100);
		
		Produto novoProduto = new Produto();
		novoProduto.setNome("Coca-cola");
		novoProduto.setPreco(13.50);
		novoProduto.setQuantidade(100);
		
		Mockito.when(produtoDAO.salvar(novoProduto)).thenReturn(produtoMock);
		
		ProdutoBusiness produtoBusiness = new ProdutoBusiness(produtoDAO);
		
		Produto produtoSalvar = produtoBusiness.salvarProduto(novoProduto);
		
		assertEquals(produtoMock.getId(), produtoSalvar.getId());
	}
	
	@Test
	public void salvarProdutoErroNomeVazio() {
		Produto novoProduto = new Produto();
		novoProduto.setNome("");
		novoProduto.setPreco(13.50);
		novoProduto.setQuantidade(100);
		
		ProdutoBusiness produtoBussiness = new ProdutoBusiness(produtoDAO);
		
		try {
			produtoBussiness.salvarProduto(novoProduto);
		} catch(RuntimeException e) {
			assertEquals(e.getMessage(), "Nome requerido.");
		}
	}
	
	@Test
	public void salvarProdutoErroPrecoVazio() {
		Produto novoProduto = new Produto();
		novoProduto.setNome("Coca-cola");
		novoProduto.setPreco(null);
		novoProduto.setQuantidade(100);
		
		ProdutoBusiness produtoBussiness = new ProdutoBusiness(produtoDAO);
		
		try {
			produtoBussiness.salvarProduto(novoProduto);
		} catch(RuntimeException e) {
			assertEquals(e.getMessage(), "Pre�o requerido.");
		}
	}
	
	@Test
	public void salvarProdutoErroQuantidadeVazio() {
		Produto novoProduto = new Produto();
		novoProduto.setNome("Coca-cola");
		novoProduto.setPreco(13.50);
		novoProduto.setQuantidade(null);
		
		ProdutoBusiness produtoBussiness = new ProdutoBusiness(produtoDAO);
		
		try {
			produtoBussiness.salvarProduto(novoProduto);
		} catch(RuntimeException e) {
			assertEquals(e.getMessage(), "Quantidade requerido.");
		}
	}
	
	@Test
	public void deletarEmpresa() {
		Produto produtoMock = null;
		
		Produto novoProduto = new Produto();
		novoProduto.setId(Long.valueOf(99));
		novoProduto.setNome("Coca-cola");
		novoProduto.setPreco(13.50);
		novoProduto.setQuantidade(100);
		
		Mockito.when(produtoDAO.excluirPorduto(novoProduto)).thenReturn(produtoMock);
		
		ProdutoBusiness produtoBusiness = new ProdutoBusiness(produtoDAO);
		
		Produto produtoDeleta = produtoBusiness.deletarProduto(novoProduto);
		
		assertEquals(produtoMock, produtoDeleta);
	}
	
}
