package br.edu.facef.tra;

import br.edu.facef.model.Cliente;

public class main {

	public static void main(String[] args) {
		Cliente novoCliente = new Cliente();
		novoCliente.setId(Long.valueOf(99));
		novoCliente.setNome("Maria da Silva");
		novoCliente.setEstado("MG");
		novoCliente.setCidade("Monte Santo de Minas");
		
		System.out.print(novoCliente.toString());
	}

}
