package br.edu.facef.dao;

import br.edu.facef.model.Cliente;

public class ClienteDAO {
	
	public Cliente salvar(Cliente cliente) {
		cliente.setId(999l);
		return cliente;
	}
	
	public Cliente excluir(Cliente cliente) {
		cliente = new Cliente();
		return cliente;
	}
	
	public Cliente editar(Cliente cliente) {
		cliente = new Cliente();
		cliente.setId(Long.valueOf(99));
		cliente.setNome("Jo�o Marcos");
		cliente.setCidade("Franca");
		cliente.setEstado("SP");
		return cliente;
	}

}
