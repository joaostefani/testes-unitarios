package br.edu.facef.dao;

import java.util.Date;

import br.edu.facef.model.Cliente;
import br.edu.facef.model.Empresa;
import br.edu.facef.model.Produto;
import br.edu.facef.model.Venda;
import br.edu.facef.model.Vendedor;

public class VendaDAO {
	
	public Venda salvar(Venda venda) {
		venda.setId(999l);
		return venda;
	}
	
	public Venda excluir(Venda venda) {
		venda = new Venda();
		return venda;
	}
	
	public Venda editar(Venda venda) {
		venda = new Venda();
		venda.setId(999l);
		
		Cliente cliente = new Cliente();
		cliente.setId(Long.valueOf(99));
		cliente.setNome("Maria da Silva");
		cliente.setCidade("Monte Santo de Minas");
		cliente.setEstado("MG");

		venda.setCliente(cliente);
		
		Empresa empresa = new Empresa();
		empresa.setId(111l);
		empresa.setNome("Irm�es da Silva");
		empresa.setCnpj("1234566");
		empresa.setTelefone("1234566");
		
		venda.setEmpresa(empresa);
		
		Produto produto = new Produto();
		produto.setId(111l);
		produto.setNome("Notebook Asus");
		produto.setPreco(2000.00);
		produto.setQuantidade(10);
		
		venda.setProduto(produto);
		
		Vendedor vendedor = new Vendedor();
		vendedor.setId(111l);
		vendedor.setCpf("803.872.940-40");
		vendedor.setNome("David Stefani Prado");
		vendedor.setTelefone("16994151771");
		
		venda.setVendedor(vendedor);
		venda.setDataVenda(new Date());
		
		return venda;
	}

}
