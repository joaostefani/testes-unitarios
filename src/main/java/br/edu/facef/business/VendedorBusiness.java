package br.edu.facef.business;

import br.edu.facef.dao.VendedorDAO;
import br.edu.facef.model.Vendedor;

public class VendedorBusiness {
	
	private VendedorDAO vendedorDAO;
	
	public VendedorBusiness (VendedorDAO dao) {
		this.vendedorDAO = dao;
	}
	
	public Vendedor salvarVendedor(Vendedor vendedor) {
		if (vendedor == null || vendedor.getNome().isEmpty()) {
			throw new RuntimeException("Nome requerido.");
		}
		
		if (vendedor == null || vendedor.getCpf().isEmpty()) {
			throw new RuntimeException("CPF requerido.");
		}
		
		return this.vendedorDAO.salvar(vendedor);
	}
	
	public Vendedor deletarVendedor(Vendedor vendedor) {
		if (vendedor == null || vendedor.getId() == null) {
			throw new RuntimeException("N�o � possivel deletar o vendedor!");
		}
		
		return this.vendedorDAO.excluirVendedor(vendedor);
	}
}
