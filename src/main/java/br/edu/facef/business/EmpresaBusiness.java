package br.edu.facef.business;

import br.edu.facef.dao.EmpresaDAO;
import br.edu.facef.model.Empresa;

public class EmpresaBusiness {
	
	private EmpresaDAO empresaDAO;
	
	public EmpresaBusiness(EmpresaDAO dao) {
		this.empresaDAO = dao;
	}
	
	public Empresa salvarEmpresa(Empresa empresa) {
		if (empresa == null || empresa.getNome().isEmpty()) {
			throw new RuntimeException("Nome requerido.");
		}
		
		if (empresa == null || empresa.getCnpj().isEmpty()) {
			throw new RuntimeException("CNPJ requerido.");
		}

		return this.empresaDAO.salvar(empresa);
	}
	
	
	public Empresa deletarEmpresa(Empresa empresa) {
		
		if (empresa == null || empresa.getId() == null) {
			throw new RuntimeException("N�o � possivel deletar o empresa!");
		}	
		
		return this.empresaDAO.excluirEmpresa(empresa);
		
	}
}
