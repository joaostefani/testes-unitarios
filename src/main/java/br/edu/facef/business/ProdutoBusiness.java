package br.edu.facef.business;

import br.edu.facef.dao.ProdutoDAO;
import br.edu.facef.model.Produto;

public class ProdutoBusiness {
	
	private ProdutoDAO produtoDAO;
	
	public ProdutoBusiness (ProdutoDAO dao) {
		this.produtoDAO = dao;
	}
	
	public Produto salvarProduto(Produto produto) {
		if (produto == null || produto.getNome().isEmpty()) {
			throw new RuntimeException("Nome requerido.");
		}
		
		if (produto == null || produto.getQuantidade() == null) {
			throw new RuntimeException("Quantidade requerido.");
		}
		
		if (produto == null || produto.getPreco() == null) {
			throw new RuntimeException("Pre�o requerido.");
		}

		return this.produtoDAO.salvar(produto);
	}
	
	public Produto deletarProduto(Produto produto) {
		if (produto == null || produto.getId() == null) {
			throw new RuntimeException("N�o � possivel deletar o empresa!");
		}	
		
		return this.produtoDAO.excluirPorduto(produto);
	}
}
