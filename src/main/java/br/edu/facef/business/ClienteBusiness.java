package br.edu.facef.business;

import br.edu.facef.dao.ClienteDAO;
import br.edu.facef.model.Cliente;

public class ClienteBusiness {
	private ClienteDAO clienteDAO;

	public ClienteBusiness(ClienteDAO dao) {
		this.clienteDAO = dao;
	}

	public Cliente salvarCliente(Cliente cliente) {
		String message = validarCliente(cliente);
		
		if (message != null) {
			throw new RuntimeException(message);
		}
		
		return this.clienteDAO.salvar(cliente);
	}
	
	
	public Cliente deletarCliente(Cliente cliente) {
		String message = validarClienteIdCliente(cliente);
		
		if (message != null) {
			throw new RuntimeException(message);
		}
		
		return this.clienteDAO.excluir(cliente);
	}
	
	public Cliente editarCliente(Cliente cliente) {
	String message = validarClienteIdCliente(cliente);
		
		if (message != null) {
			throw new RuntimeException(message);
		}
		
		return this.clienteDAO.editar(cliente);
	}
	
	//--------------------------------------------------------------------------------------------
	public String validarClienteIdCliente(Cliente cliente) {
		
		String message = null;
		
		if (cliente == null || cliente.getId() == null) {
			message = "N�o � possivel deletar o cliente!";
		}
		
		return message;
	}

	public String validarCliente(Cliente cliente) {

		String message = null;

		if (cliente == null || cliente.getNome().isEmpty()) {
			message = "Nome do cliente invalido!";
		}

		if (cliente == null || cliente.getEstado().isEmpty()) {
			message = "Estado do cliente invalido!";
		}

		if (cliente == null || cliente.getCidade().isEmpty()) {
			message = "Cidade do cliente invalido!";
		}

		return message;
	}
}
