package br.edu.facef.business;

import br.edu.facef.dao.VendaDAO;
import br.edu.facef.model.Venda;

public class VendaBusiness {
	
	private VendaDAO vendaDAO;

	public VendaBusiness(VendaDAO vendaDAO) {
		super();
		this.vendaDAO = vendaDAO;
	}
	
	public Venda salvarVenda(Venda venda) {
		String message = validarVenda(venda);
		
		if (message != null) {
			throw new RuntimeException(message);
		}
		
		return this.vendaDAO.salvar(venda);
	}
	
	
	public Venda deletarVenda(Venda venda) {
		String message = validarVendaId(venda);
		
		if (message != null) {
			throw new RuntimeException(message);
		}
		
		return this.vendaDAO.excluir(venda);
		
	}
	
	
	public Venda editarVenda(Venda venda) {
		String message = validarVendaId(venda);
		
		if (message != null) {
			throw new RuntimeException(message);
		}
		
		return this.vendaDAO.editar(venda);
		
	}
	
	
	public String validarVendaId(Venda venda) {
		String message = null;
		
		if (venda == null || venda.getId() == null) {
			message = "N�o � possivel deletar a venda!";
		}
		
		return message;
	}
	
	public String validarVenda(Venda venda) {
		String message = null;
		
		if (venda == null || venda.getCliente() == null) {
			message = "Cliente da venda invalido!";
		}
		
		if (venda == null || venda.getEmpresa() == null) {
			message = "Empresa da venda invalida!";
		}
		
		if (venda == null || venda.getProduto() == null) {
			message = "Produto da venda invalida!";
		}
		
		if (venda == null || venda.getVendedor() == null) {
			message = "Vendedor da venda invalida!";
		}
		
		if (venda == null || venda.getDataVenda() == null) {
			message = "Data da venda invalida!";
		}
		
		return message;
	}
	

}
