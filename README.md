## Requisitos

1. docker [www.docker.com/get-started]
2. eclipse [https://www.eclipse.org/downloads/]

## Passo a passo - Executar os comandos dentro da pasta do projeto

1. docker build . -t [nome-da-imagem]
2. docker run [nome-da-imagem]